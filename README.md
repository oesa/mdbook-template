# mdBook Template

A repository template to get started writing an online book in Markdown, with [mdBook](https://github.com/rust-lang/mdBook).

## What it is

Using this template, you can immediately start writing your online book in [Markdown](https://en.wikipedia.org/wiki/Markdown).

The template contains:

* a small **[mdBook](https://github.com/rust-lang/mdBook) project** that shows some important features of the book. You can replace the content with your book's content.
* a **Continuous Integration (CI) [script](https://git.ufz.de/oesa/mdbook-template/-/blob/main/.gitlab-ci.yml)** that automatically builds and publishes the book on every push to / merge into branch `main`.

To give an impression of how a published book looks like, [here](https://oesa.pages.ufz.de/mdbook-template) is the rendered version of this template book.

## How to use it

To use the template for an own project, you need to fork it, remove the fork relationship, and rename the project:

**1. Fork the project**

At the top right of this repository's page, press `fork` to create a fork of this project (i.e. a clone in another namespace):

![image](/uploads/3670f52af94ab789da40869d769cc14b/image.png)

Select a "namespace" (yourself, or a group).

You should then be brought to the fork's page. It differs from the original repository by the breadcrumb `<Your name> > mdBook-Template` at the very top, compared to `OESA > mdBook-Template`. Further, below the summary there is a line `Forked from OESA / mdBook-Template`.

**2. Rename the project**

Again under `Settings > General > Advanced`, go to section `Change path` and do just that. At the top of `Settings > General`, rename the repository accordingly (and press `Save changes`).

Edit the file `book.toml` to enter book title and author names.

**3. Set visibility**

If you want to have your book publicly visible, not only to project or group members, change the visibility as follows.

Go to `Settings > General` and scroll down to section `Visibility, project features, permissions`. Unfold it. At the very top, you can change to visibility of the project sources. Further down (2nd last item), the visibility of the rendered book can be changed separately.

**4. Write your book**

You can now write your book using Markdown, starting from the files in directory `src`. There are two possible ways to do so:

* Clone the repository locally. Write locally, commit an push to your remote.
* Edit files online using the Web IDE. You find a button `Web IDE` in the header of your repository's page.

Read the [mdBook documentation](https://rust-lang.github.io/mdBook/) for details on how to work with mdBook.

After a first CI run (see below), the rendered HTML book is available under an address following this pattern:

```
https://<USER or GROUP>.pages.ufz.de/<REPO>
```

As an example, the rendered page of this project can be found under https://oesa.pages.ufz.de/mdbook-template/

The CI does not run automatically after forking. To run it, push something to branch `main`.

Each time someone pushes to or merges into `main`, the CI builds and publishes the book automatically.

## License

The code of this project is released into the Public Domain.

The authors of books derived from the template should create their own file `LICENSE` with appropriate license information, before publishing.

**Copyright & disclaimer**

```
Copyright (c) 2021 Helmholtz-Zentrum für Umweltforschung GmbH – UFZ  
The code is a property of:  
  Helmholtz-Zentrum für Umweltforschung GmbH – UFZ  
  Registered Office: Leipzig  
  Registration Office: Amtsgericht Leipzig  
  Trade Register Nr. B 4703  
  Chairman of the Supervisory Board: MinDirig'in Oda Keppler  
  Scientific Director: Prof. Dr. Georg Teutsch  
  Administrative Director: Dr. Sabine König

Authors and contact:  
  Martin Lange (martin.lange@ufz.de)  
  Permoserstraße 15, D-04318 Leipzig, Germany

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```

## Further reading

* [mdBook documentation](https://rust-lang.github.io/mdBook/)

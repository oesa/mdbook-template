# MdBook features

This chapter briefly describes some notable features of mdBook. For a full list, see the [mdBook documentation](https://rust-lang.github.io/mdBook).

## Table of content / navigation

mdBook supports numbered and unnumbered chapters as well as sub-chapters in the navigation at the left. Details are explained in chapter [SUMMARY.md](https://rust-lang.github.io/mdBook/format/summary.html) of the documentation.

## Math formulas

mdBook requires to explicitly enable LaTeX math support in the `book.toml` config file through

```toml
[output.html]
mathjax-support = true
```

Inline equations must be enclosed in `\\(...\\)`: \\(E = m c^2\\).

Math blocks use `\\[` and `\\]` as delimiters:

```markdown
\\[
\sum_{i = m}^n a_i = a_m + a_{m+1} + a_{m+2} +\cdots+ a_{n-1} + a_n
\\]
```

Gives:

\\[
\sum_{i = m}^n a_i = a_m + a_{m+1} + a_{m+2} +\cdots+ a_{n-1} + a_n
\\]

Unfortunately, this syntax is not supported by GitLab, so there will be no proper preview in the Web IDE.

## Include code from files

mdBook supports including code from files, even only specific lines. For details, see section [mdBook-specific features](https://rust-lang.github.io/mdBook/format/mdbook.html) in the documentation

## Other features

* Search functionality at the top of the book
* Books can be styled using custom CSS files

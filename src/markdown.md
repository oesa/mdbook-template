# Markdown introduction

This chapter gives a brief overview of the Markdown syntax for beginners. For details and a full list of syntax elements, see the [Markdown Guide](https://www.markdownguide.org/basic-syntax/).

## Basic syntax

**Headings of different level**

```markdown
# Heading 1 / Title

## Heading 2

### Heading 3

#### ...
```

**Emphasis**

| Markdown            | Rendered output   |
|---------------------|-------------------|
| `**bold**`          | **bold**          |
| `*italic*`          | *italic*          |
| `***bold italic***` | ***bold italic*** |

**Links**

```markdown
A [link](http://example.com) example.
```

A [link](http://example.com) example.

----

Further formatting options are block quotes, numbered an bulleted lists,
inclusion of images, etc. See the [Markdown Guide](https://www.markdownguide.org/basic-syntax/).

## Code blocks

Markdown supports inline code and code blocks with syntax highlighting.

Inine code must be surrounded with back-ticks. e.g. `` `variable_name` `` gives `variable_name`.

Code must be surrounded by triple backticks (` ``` `). A language for syntax highlighting can be given immediately after the opening backtick sequence:

````markdown
```java
class Main {
    public static void main(String[] args) {

    }
}
```
````

Renders as:

```java
class Main {
    public static void main(String[] args) {

    }
}
```

For math formulas in LaTeX syntax, see chapter [MdBook features](./mdbook-features.md).

## Tables

Tables are not supported by all Markdown dialects, but can be used with mdBook.

```markdown
| Column 1 | Column 2 |
|----------|----------|
| A        | B        |
| C        | D        |
```

| Column 1 | Column 2 |
|----------|----------|
| A        | B        |
| C        | D        |

# MdBook CLI usage

This page explains the most important mdBook command line arguments. For a full list, see chapter [Command Line Tool](https://rust-lang.github.io/mdBook/cli/index.html) of the documentation.

## Installation

See the mdBook [README](https://github.com/rust-lang/mdBook) on GitHub.

## Editing locally

While editing locally, you can use a browser as a live preview of your rendered Markdown. In the root folder of the book, run

```
mdbook serve
```

Open the address http://localhost:3000 in a browser, as indicated in the output.

Alternatively, this command opens the browser automatically:

```
mdbook serve --open
```

Whenever you save edits to the book's files, the browser view will refresh.
